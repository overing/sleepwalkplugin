package org.overwork.mcp;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 夢遊插件
 * @author overing
 */
public class SleepwalkPlugin extends JavaPlugin implements Listener {

	/** 設定檔 Key: 夢遊距離(相對於床 最遠會到) */
	private static final String PATH_DISTANCE_FROM_BED = "DistanceFromBed";
	
	/** 設定檔 Key: 發生機率(%) */
	private static final String PATH_INCIDENCE = "Incidence";

	/** 設定檔 Key: 夢遊發生訊息(夢遊發生後要給玩家看的訊息) */
	private static final String PATH_MESSAGE = "Message";
	
	/** 取亂數的工具 */
	private final Random random = new Random();
	
	/** 插件執行之後實際的夢遊距離 */
	private int distance = 32;

	/** 插件執行之後實際的發生機率 */
	private int incidence = 5;
	
	private String message = "<Sleepwalk>";

	@Override
	public void onEnable() {
		getLogger().info("onEnable()");

		// 跟插件管理器註冊自己
		getServer().getPluginManager().registerEvents(this, this);
		
		// 拿出設定檔
		FileConfiguration config = getConfig();
		// 加入預設值 夢遊距離
		config.addDefault(PATH_DISTANCE_FROM_BED, distance);
		// 加入預設值 發生機率
		config.addDefault(PATH_INCIDENCE, incidence);
		// 加入預設值 發生訊息
		config.addDefault(PATH_MESSAGE, message);
		// 從預設值複製設定
		config.options().copyDefaults(true);
		// 存檔 (到這邊為止是為了要生成有預射值的設定檔出來)
		saveConfig();
		
		// 從設定檔重新讀入設定
		reloadConfig();
		
		// 從設定拿出距離
		distance = config.getInt(PATH_DISTANCE_FROM_BED);
		// 印出距離確認
		getLogger().info(PATH_DISTANCE_FROM_BED + "=" + distance);

		// 從設定拿出機率
		incidence = config.getInt(PATH_INCIDENCE);
		// 印出機率確認
		getLogger().info(PATH_INCIDENCE + "=" + incidence);

		// 從設定拿出訊息
		message = config.getString(PATH_MESSAGE);
		// 印出訊息確認
		getLogger().info(PATH_MESSAGE + "=" + message);
	}

	@EventHandler
	public void onPlayerLeaveBed(PlayerBedLeaveEvent e) {
		getLogger().warning("onPlayerLeaveBed(" + e + ")");
		
		// 取一個隨機數字範圍從 0 ~ 100
		// 如果大於發生率 就直接 return (後面的程式都不做了)
		if (random.nextInt(100) > incidence)
			return;

		// 從事件裡拿出 {床}
		Block bed = e.getBed();
		// 從床裡拿出 {床的位置}
		Location bedLocation = bed.getLocation();
		// 印出來確認一下
		getLogger().warning("bed on " + bedLocation);
		
		// 先算出 x 跟 z 要在哪裡
		// 大致上公式是
		//    隨機 0 ~ (設定的距離 x 2) - 設定的距離 + 床的位置
		final int R = distance * 2;
		int x = (int) (random.nextInt(R) - distance + bedLocation.getX());
		int z = (int) (random.nextInt(R) - distance + bedLocation.getZ());
		
		// 從床裡拿出床所在的 {世界}
		World world = bed.getWorld();
		// 用世界去找 在那個 x, z 的位置 最高有方塊實體的 y
		// 加上 3 是怕玩家卡住 (再拉高一點就可以摔玩家了www 不過那樣就不像夢遊了)
		int y = world.getHighestBlockYAt(x, z) + 3;
		
		// 包好玩家的新 {位置}
		Location playerLocation = new Location(world, x, y, z);
		// 印出來確認一下
		getLogger().warning("teleport player to " + playerLocation);

		// 從事件裡拿出準備起床的玩家
		Player player = e.getPlayer();
		// 把玩家傳送到算出來的新位置
		player.teleport(playerLocation);
		
		// 如果訊息不是空的 就顯示給玩家看
		if (message != null && !message.isEmpty())
			player.sendMessage(message);
	}

	@Override
	public void onDisable() {
		getLogger().info("onDisable()");
	}

}
